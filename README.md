# INTRODUCTION

This module implements the [JSNLog](http://js.jsnlog.com) library.
And stores javascript messages in watchdog.

# INSTALLATION / REQUIREMENTS

* Preferred option: composer require drupal/jsnlog.
  * For the JSNLog javascript library add:
  ```
    {
        "type": "composer",
        "url": "https://asset-packagist.org"
    }
  ```
  * extend your libraries path under installer-path:
  ```
    "web/libraries/{$name}": [
        "type:drupal-library",
        "type:bower-asset",
        "type:npm-asset"
    ],
  ```
  * add installer-types under extra:
   ```
    "installer-types": ["bower-asset", "npm-asset"],
   ```
   * and finally:
   ```
     composer require bower-asset/jsnlog
   ```
* Manually: Place the module in the modules folder.
  * Download JSNLog library from [JSNLog](http://js.jsnlog.com) and place
    the folder under webroot/libraries named jsnlog.
  * (Optional - for better output) Download WhichBrowser library from
    [github](https://github.com/WhichBrowser/Parser-PHP) and place
    the folder under webroot/libraries named whichbrowser.
* Activate and visit settings page.

# CONFIGURATION

* Enable or disable it.
* Choose logging type: if ajax, console or both.
* Choose library type.
* Debug level to save into watchdog.
* User agent filter to log only specific browsers.
* Include or exclude roles and or pages.

# USAGE

If a javascript error occurs. The event will trigger an ajax callback and
the error will be listed in watchdog.

# TROUBLESHOOT

Be beware of your ad blocker. This may block the JSNLog script if
aggregation is off, or you use the CDN option.

# Custom error triggers

For your custom javascript you may want to use some other notifications from
the JSNLog library.

``` javascript
JL().warn("warn message");
JL().info("info message");
JL().fatal("fatal message");
```

Depending on your debug level you configured. These messages will
be tracked as well.

# Error mapping from JSNLog to watchdog

---------------------<br>
| JSNLog | Watchdog |<br>
---------------------<br>
| < 2000 | log      |<br>
| < 3000 | debug    |<br>
| < 4000 | info     |<br>
| < 5000 | warning  |<br>
| > 4999 | error    |<br>
---------------------<br>

See:
[NumericSeverities](http://js.jsnlog.com/Documentation/HowTo/NumericSeverities)

# Versions

* For Drupal 8.8 and Drupal 9 upwards use 8.x-1.1
* For older Drupal 8 versions use 8.x-1.0

# Further Information

* http://js.jsnlog.com/Documentation/JSNLogJs

# Run Tests

```
chromedriver --port=4444
cp core/phpunit.xml.dist ./phpunit.xml
// Modify xml
./vendor/bin/phpunit -c . modules/jsnlog
```
