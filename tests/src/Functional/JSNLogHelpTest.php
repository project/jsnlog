<?php

namespace Drupal\Tests\jsnlog\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Test jsnlog settings form.
 *
 * @group jsnlog
 */
class JSNLogHelpTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['jsnlog', 'help'];

  /**
   * A simple user.
   *
   * @var \Drupal\user\Entity\User
   */
  private $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp():void {
    parent::setup();

    $this->user = $this->drupalCreateUser([
      'access administration pages',
      'administer jsnlog',
      'access help pages',
    ]);
  }

  /**
   * Test help page.
   */
  public function testHelpPage() {
    $this->drupalLogin($this->user);

    $path = Url::fromRoute('help.page', ['name' => 'jsnlog']);
    $this->drupalGet($path);

    $statusCode = $this->getSession()->getDriver()->getClient()->getResponse()->getStatusCode();
    $this->assertEquals(200, $statusCode);

    $this->assertSession()->pageTextContains('JSNLog');
  }

}
