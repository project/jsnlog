<?php

namespace Drupal\Tests\jsnlog\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Test jsnlog settings form.
 *
 * @group jsnlog
 */
class JSNLogAjaxLogTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['jsnlog', 'dblog'];

  /**
   * A simple user.
   *
   * @var \Drupal\user\Entity\User
   */
  private $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp():void {
    parent::setup();

    $this->user = $this->drupalCreateUser([
      'access content',
    ]);
  }

  /**
   * Test help page.
   */
  public function testErrorPost() {
    $this->drupalLogin($this->user);

    $front_url = Url::fromRoute('<front>');
    $this->drupalGet($front_url);

    $url = Url::fromRoute('jsnlog.ajax_log');
    $token = $this->container->get('csrf_token')->get($url->getInternalPath());
    $url->setOptions(['absolute' => TRUE, 'query' => ['token' => $token]]);

    $this->getSession()->getDriver()->getClient()->request('POST', $url->toString(), [
      'form_params' => $this->getErrorPostData($front_url),
    ]);

    $statusCode = $this->getSession()->getDriver()->getClient()->getResponse()->getStatusCode();
    $this->assertEquals(200, $statusCode);
  }

  /**
   * Get error post data.
   *
   * @param Drupal\Core\Url $url
   *   The url object to add to log message.
   *
   * @return array
   *   The post array to log.
   */
  private function getErrorPostData(Url $url):array {
    $url->setOptions(['absolute' => TRUE]);
    $url = $url->toString();
    return [
      "href" => $url,
      "lg" => [
        [
          "l" => 6000,
          "m" => [
            "@msg" => "Uncaught Exception",
            "@errorMsg" => "Uncaught ReferenceError: variableName is not defined",
            "@url" => "{$url}/core/misc/drupal.js?v=8.7.3",
            "@line_number" => 13,
            "@column" => 7,
            "stack" => "ReferenceError: variableName is not defined\n    at Object.attach ({$url}/modules/custom/jsnlog/js/jsnlog.js?v=8.7.3:17:7)\n    at {$url}/core/misc/drupal.js?v=8.7.3:25:24\n    at Array.forEach (<anonymous>)\n    at Object.Drupal.attachBehaviors ({$url}/core/misc/drupal.js?v=8.7.3:22:34)\n    at {$url}/core/misc/drupal.init.js?v=8.7.3:16:12\n    at HTMLDocument.t ({$url}/core/assets/vendor/domready/ready.min.js?v=1.0.8:4:381)",
            "@href" => $url,
            "@windowWidth" => 1920,
            "@windowHeight" => 386,
            "@windowPositionX" => 0,
            "@windowPositionY" => 208,
          ],
          "n" => "drupalOnErrorLogger",
          "t" => time(),
          "u" => 1,
        ],
      ],
    ];
  }

}
