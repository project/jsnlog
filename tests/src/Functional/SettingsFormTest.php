<?php

namespace Drupal\Tests\jsnlog\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Test jsnlog settings form.
 *
 * @group jsnlog
 */
class SettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['jsnlog'];

  /**
   * Default config route.
   *
   * @var string
   */
  private static $route = 'jsnlog.settings';

  /**
   * Default submit button.
   *
   * @var string
   */
  public static $button = 'Save configuration';

  /**
   * A simple user.
   *
   * @var \Drupal\user\Entity\User
   */
  private $user;

  /**
   * A simple user.
   *
   * @var \Drupal\Core\Url
   */
  private $path;

  /**
   * {@inheritdoc}
   */
  protected function setUp():void {
    parent::setup();

    \Drupal::service('jsnlog.download')->download();

    $this->user = $this->drupalCreateUser(['administer jsnlog']);
    $this->drupalLogin($this->user);

    $this->path = Url::fromRoute(static::$route);
    $this->drupalGet($this->path);
  }

  /**
   * Test Build Form.
   */
  public function testForm() {
    $this->assertSession()->pageTextContains('JSNLog');
    $this->submitForm([], static::$button);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('JSNLog');
  }

  /**
   * Test validation.
   */
  public function testFormValidation() {
    // Force validation Error.
    $fail_path = 'foo';
    $this->submitForm([
      'visibility_request_path_pages' => $fail_path,
    ], static::$button);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains("Path \"{$fail_path}\" not prefixed with slash.");
  }

  /**
   * Test configuration.
   */
  public function testFormConfiguration() {
    $this->assertSession()->responseContains('jsnlog.min.js');
    $enabled = $this->config('jsnlog.settings')->get('enabled');
    $this->assertEquals(1, $enabled);
    $this->submitForm([
      'enabled' => '0',
      'visibility_request_path_pages' => '',
      'library_type' => 'ext',
    ], static::$button);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('The configuration options have been saved.');
    $enabled = $this->config('jsnlog.settings')->get('enabled');
    $this->assertEquals(0, $enabled);

    $this->assertSession()->responseNotContains('/libraries/jsnlog/jsnlog.min.js');
    $this->assertSession()->responseContains('jsnlog.min.js');

    $this->submitForm([
      'enabled' => '1',
      'visibility_request_path_pages' => '',
      'library_type' => 'min',
    ], static::$button);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains('/libraries/jsnlog/jsnlog.min.js');
  }

}
