<?php

namespace Drupal\Tests\jsnlog\Kernel;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Url;
use Drupal\KernelTests\KernelTestBase;
use Drupal\jsnlog\Access\AjaxLogAccess;

/**
 * Test jsnlog log access.
 *
 * @group jsnlog
 */
class JSNLogAjaxLogAccessTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['path_alias', 'jsnlog', 'system'];

  /**
   * The jsnlog config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $jsnlogConfig;

  /**
   * The log access service.
   *
   * @var \Drupal\jsnlog\Access\AjaxLogAccess
   */
  protected $logAccessService;

  /**
   * The account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * {@inheritdoc}
   */
  protected function setUp():void {
    parent::setUp();
    $this->installConfig(static::$modules);
    $this->installEntitySchema('path_alias');
    // Get config.
    $this->jsnlogConfig = $this->config('jsnlog.settings');
    // Disable for settings form.
    $formPath = Url::fromRoute('jsnlog.settings')->toString();
    $this->jsnlogConfig->set('visibility.request_path_pages', $formPath);
    $this->jsnlogConfig->save();
    $this->logAccessService = $this->container->get('jsnlog.ajax_log_access');
    // Get account.
    $this->account = $this->container->get('current_user');
  }

  /**
   * Test class and basic access method response.
   */
  public function testAccess() {
    $this->assertInstanceOf(AjaxLogAccess::class, $this->logAccessService);

    // Check if return result is instance of AccessResult.
    $accessResult = $this->logAccessService->access($this->account);
    $this->assertInstanceOf(AccessResult::class, $accessResult);

    // AccessResult should be false because it is not the correct route.
    $this->assertEquals(AccessResult::allowed(), $accessResult);
  }

  /**
   * Test visibility page method.
   */
  public function testVisibilityPages() {
    $path = Url::fromRoute('jsnlog.ajax_log')->toString();
    $this->assertEquals($this->logAccessService->visibilityPages($path), TRUE);

    $path = Url::fromRoute('jsnlog.settings')->toString();
    $this->assertEquals($this->logAccessService->visibilityPages($path), FALSE);
  }

  /**
   * Test user roles role  method.
   */
  public function testVisibilityRoles() {
    $this->assertEquals($this->logAccessService->visibilityRoles($this->account), TRUE);
    // Only for authenticated.
    $this->jsnlogConfig->set('visibility.user_role_roles', ['authenticated' => 'authenticated']);
    $this->jsnlogConfig->save();
    $this->assertEquals($this->logAccessService->visibilityRoles($this->account), FALSE);
  }

}
