<?php

namespace Drupal\Tests\jsnlog\FunctionalJavascript;

use Drupal\Core\Url;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * JavaScript tests.
 *
 * @ingroup jsnlog
 *
 * @group jsnlog
 */
class JSNLogJsTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['jsnlog', 'dblog'];

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * Tests that the home page loads with a 200 response.
   */
  public function testFrontpage() {
    $database = \Drupal::database();
    $query = $database->select('watchdog', 'w')->countQuery();
    $this->assertEquals(1, $query->execute()->fetchField());

    $this->drupalGet(Url::fromRoute('<front>'));
    // Wait for a short while.
    $this->getSession()->wait(2000);

    $this->assertJsCondition('typeof(JL) == \'function\'');
    $this->assertJsCondition('typeof(JL().error(\'This is a test error.\')) == \'object\'');
    // Wait for a short while.
    $this->getSession()->wait(2000);

    $query = $database->select('watchdog', 'w')->countQuery();
    $this->assertEquals(2, $query->execute()->fetchField());
  }

}
