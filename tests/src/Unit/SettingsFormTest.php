<?php

declare(strict_types=1);

namespace Drupal\Tests\jsnlog\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\jsnlog\Form\SettingsForm;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Tests settings form.
 *
 * @coversDefaultClass \Drupal\jsnlog\Form\SettingsForm
 *
 * @group jsnlog
 */
class SettingsFormTest extends UnitTestCase {

  /**
   * The settings form.
   *
   * @var \Drupal\jsnlog\Form\SettingsForm
   */
  protected $form;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The library discovery.
   *
   * @var \Drupal\Core\Extension\LibraryDiscoveryInterface
   */
  protected $libraryDiscovery;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The typed config.
   *
   * @var \Drupal\Core\Config\TypedConfigManagerInterface
   */
  protected $typedConfig;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $container = new ContainerBuilder();

    $string_translation = $this->getStringTranslationStub();
    $container->set('string_translation', $string_translation);

    $this->configFactory = $this->createMock('Drupal\Core\Config\ConfigFactoryInterface');
    $container->set('config.factory', $this->configFactory);

    $this->messenger = $this->createMock('Drupal\Core\Messenger\MessengerInterface');
    $container->set('messenger', $this->messenger);

    $this->libraryDiscovery = $this->createMock('Drupal\Core\Asset\LibraryDiscovery');
    $container->set('library.discovery', $this->libraryDiscovery);

    $this->fileSystem = $this->createMock('Drupal\Core\File\FileSystem');
    $container->set('file_system', $this->fileSystem);

    $this->entityTypeManager = $this->createMock('Drupal\Core\Entity\EntityTypeManagerInterface');
    $container->set('entity_type.manager', $this->entityTypeManager);

    $this->typedConfig = $this->createMock('Drupal\Core\Config\TypedConfigManagerInterface');
    $container->set('config.typed', $this->typedConfig);

    \Drupal::setContainer($container);

    $this->form = SettingsForm::create($container);
  }

  /**
   * Test the create method.
   *
   * @covers ::create
   */
  public function testCreate() {
    $container = \Drupal::getContainer();

    $form = SettingsForm::create($container);
    $this->assertInstanceOf('Drupal\jsnlog\Form\SettingsForm', $form);
  }

  /**
   * Test the constructor.
   *
   * @covers ::__construct
   */
  public function testConstructor() {
    $form = new SettingsForm(
      $this->configFactory,
      $this->messenger,
      $this->libraryDiscovery,
      $this->fileSystem,
      $this->entityTypeManager,
      $this->typedConfig
    );

    $this->assertInstanceOf('Drupal\jsnlog\Form\SettingsForm', $form);
  }

  /**
   * Test the getFormId method.
   *
   * @covers ::getFormId
   */
  public function testGetFormId() {
    $this->assertEquals('jsnlog_settings_form', $this->form->getFormId());
  }

  /**
   * Test the buildForm method.
   *
   * @covers ::buildForm
   * @covers ::getEditableConfigNames
   * @covers ::userRoleNames
   * @covers ::getLogTypeOptions
   * @covers ::getDebugLevelOptions
   * @covers ::getLibraryTypeOptions
   */
  public function testBuildForm() {

    $user_role = $this->createMock('Drupal\user\RoleInterface');
    $user_role->expects($this->once())
      ->method('label')
      ->willReturn('A role');
    $user_role_storage = $this->createMock('Drupal\Core\Entity\EntityStorageInterface');
    $user_role_storage->expects($this->once())
      ->method('loadMultiple')
      ->willReturn(['a_role' => $user_role]);

    $this->entityTypeManager->expects($this->once())
      ->method('getStorage')
      ->with('user_role')
      ->willReturn($user_role_storage);

    $settings = $this->createMock('Drupal\Core\Config\Config');
    $settings->expects($this->exactly(12))
      ->method('get')
      ->willReturnMap([
        ['library_type', ''],
        ['enabled', TRUE],
        ['logging_type', 'all'],
        ['debug_level', 5000],
        ['user_agent_regex', ''],
        ['visibility.request_path_pages', ''],
        ['visibility.request_path_mode', 0],
        ['visibility.user_role_roles', []],
        ['visibility.user_role_mode', 0],
      ]);

    $this->configFactory->expects($this->once())
      ->method('getEditable')
      ->with('jsnlog.settings')
      ->willReturn($settings);
    $form = [];
    $form_state = $this->createMock('Drupal\Core\Form\FormStateInterface');
    $form = $this->form->buildForm($form, $form_state);

    $this->assertCount(12, $form);

    $this->assertArrayHasKey('advanced', $form);
    $this->assertArrayHasKey('general', $form);
    $this->assertArrayHasKey('page_visibility_settings', $form);
    $this->assertArrayHasKey('role_visibility_settings', $form);
    $this->assertArrayHasKey('test_settings', $form);
    $this->assertArrayHasKey('actions', $form);
    $this->assertArrayHasKey('#theme', $form);
    $this->assertArrayHasKey('#process', $form);
    $this->assertArrayHasKey('#after_build', $form);

    $this->assertEquals([
      'all',
      'console',
      'ajax',
    ], array_keys($form['general']['logging_type']['#options']));

    $this->assertEquals([
      1000,
      2000,
      3000,
      4000,
      5000,
      6000,
    ], array_keys($form['debug_level']['#options']));

    $this->assertEquals([
      '',
      'min',
      'ext',
    ], array_keys($form['general']['library_type']['#options']));
  }

  /**
   * Test the buildForm method.
   *
   * @covers ::buildForm
   * @covers ::getEditableConfigNames
   * @covers ::userRoleNames
   * @covers ::getLogTypeOptions
   * @covers ::getDebugLevelOptions
   * @covers ::getLibraryTypeOptions
   */
  public function testBuildFormPathMode2() {

    $user_role = $this->createMock('Drupal\user\RoleInterface');
    $user_role->expects($this->once())
      ->method('label')
      ->willReturn('A role');
    $user_role_storage = $this->createMock('Drupal\Core\Entity\EntityStorageInterface');
    $user_role_storage->expects($this->once())
      ->method('loadMultiple')
      ->willReturn(['a_role' => $user_role]);

    $this->entityTypeManager->expects($this->once())
      ->method('getStorage')
      ->with('user_role')
      ->willReturn($user_role_storage);

    $settings = $this->createMock('Drupal\Core\Config\Config');
    $settings->expects($this->exactly(11))
      ->method('get')
      ->willReturnMap([
        ['library_type', ''],
        ['enabled', TRUE],
        ['logging_type', 'all'],
        ['debug_level', 5000],
        ['user_agent_regex', ''],
        ['visibility.request_path_pages', ''],
        ['visibility.request_path_mode', 2],
        ['visibility.user_role_roles', []],
        ['visibility.user_role_mode', 0],
      ]);

    $this->configFactory->expects($this->once())
      ->method('getEditable')
      ->with('jsnlog.settings')
      ->willReturn($settings);
    $form = [];
    $form_state = $this->createMock('Drupal\Core\Form\FormStateInterface');
    $form = $this->form->buildForm($form, $form_state);

    $this->assertCount(12, $form);

    $this->assertArrayHasKey('advanced', $form);
    $this->assertArrayHasKey('general', $form);
    $this->assertArrayHasKey('page_visibility_settings', $form);
    $this->assertArrayHasKey('role_visibility_settings', $form);
    $this->assertArrayHasKey('test_settings', $form);
    $this->assertArrayHasKey('actions', $form);
    $this->assertArrayHasKey('#theme', $form);
    $this->assertArrayHasKey('#process', $form);
    $this->assertArrayHasKey('#after_build', $form);

    $this->assertEquals([
      'all',
      'console',
      'ajax',
    ], array_keys($form['general']['logging_type']['#options']));

    $this->assertEquals([
      1000,
      2000,
      3000,
      4000,
      5000,
      6000,
    ], array_keys($form['debug_level']['#options']));

    $this->assertEquals([
      '',
      'min',
      'ext',
    ], array_keys($form['general']['library_type']['#options']));
  }

  /**
   * Test the submitForm method.
   *
   * @covers ::submitForm
   */
  public function testSubmitForm() {
    $form = [];
    $form_state = $this->createMock('Drupal\Core\Form\FormStateInterface');

    $settings = $this->createMock('Drupal\Core\Config\Config');

    $settings->expects($this->exactly(10))
      ->method('set')
      ->willReturnSelf();
    $settings->expects($this->once())
      ->method('save');

    $this->configFactory->expects($this->once())
      ->method('getEditable')
      ->with('jsnlog.settings')
      ->willReturn($settings);

    $this->libraryDiscovery->expects($this->never())
      ->method('clearCachedDefinitions');

    $this->form->submitForm($form, $form_state);
  }

  /**
   * Test the submitForm method.
   *
   * @covers ::submitForm
   */
  public function testSubmitFormLibraryChanged() {
    $form = [];
    $form_state = $this->createMock('Drupal\Core\Form\FormStateInterface');

    $settings = $this->createMock('Drupal\Core\Config\Config');
    $settings->expects($this->once())
      ->method('get')
      ->with('library_type')
      ->willReturn('ext');
    $settings->expects($this->exactly(10))
      ->method('set')
      ->willReturnSelf();
    $settings->expects($this->once())
      ->method('save');

    $this->configFactory->expects($this->once())
      ->method('getEditable')
      ->with('jsnlog.settings')
      ->willReturn($settings);

    $this->libraryDiscovery->expects($this->once())
      ->method('clearCachedDefinitions');

    $this->form->submitForm($form, $form_state);
  }

  /**
   * Test the validateForm method.
   *
   * @covers ::validateForm
   */
  public function testValidateForm() {
    $form = [];
    $form_state = $this->createMock('Drupal\Core\Form\FormStateInterface');
    $form_state->expects($this->exactly(6))
      ->method('getValue')
      ->willReturnMap([
        ['visibility_request_path_pages', NULL, 'apples\n\noranges'],
        ['visibility_request_path_mode', NULL, 1],
        ['visibility_user_role_mode', NULL, 0],
        ['visibility_user_role_roles', NULL, []],
        ['library_type', NULL, ''],
      ]);
    $form_state->expects($this->any())
      ->method('setErrorByName');

    $this->form->validateForm($form, $form_state);
  }

}
