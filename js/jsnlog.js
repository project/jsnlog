(function jsnLogLoad($, Drupal, window) {
  Drupal.behaviors.jsnlog = {
    logger: null,
    attach(context, settings) {
      // Check if library is present.
      if (typeof window.JL === 'undefined') {
        return;
      }
      // Attach event error handler.
      this.initEventErrorHandler();
      // Setup JSNLog with user settings.
      this.initLogger(settings.jsnlog, settings.path.baseUrl);
    },
    initEventErrorHandler() {
      // Override error handler with own.
      if (typeof window !== 'undefined') {
        window.onerror = function jsnLogOnError(
          errorMsg,
          url,
          lineNumber,
          column,
          errorObj,
        ) {
          window.JL('drupalOnErrorLogger').fatalException({
            '@msg': 'Uncaught Exception',
            '@errorMsg': errorMsg ? errorMsg.message || errorMsg : '',
            '@url': url,
            '@line_number': lineNumber,
            '@column': column,
            stack: errorObj.stack || '- stack unavailable -',
            '@href': window.location.href,
            '@windowWidth': window.innerWidth,
            '@windowHeight': window.innerHeight,
            '@windowPositionX': window.scrollX,
            '@windowPositionY': window.scrollY,
          });
          // Tell browser to run its own error handler as well
          return false;
        };
      }
    },
    initLogger(settings) {
      if (this.logger == null) {
        const appendOptions = {
          level: settings.debug_level,
        };
        const appended = [];

        if (settings.user_agent_regex.length) {
          appendOptions.userAgentRegex = settings.user_agent_regex;
        }

        window.JL.setOptions({
          defaultAjaxUrl: settings.defaultAjaxUrl,
          defaultBeforeSend: this.beforeSend,
        });
        this.logger = window.JL('defaultJSNLog');

        if (
          settings.logging_type === 'all' ||
          settings.logging_type === 'console'
        ) {
          appended.push(this.initAppender('ConsoleAppender', appendOptions));
        }

        if (
          settings.logging_type === 'all' ||
          settings.logging_type === 'ajax'
        ) {
          appended.push(this.initAppender('AjaxAppender', appendOptions));
        }

        this.logger.setOptions({
          appended,
        });
      }
    },
    initAppender(appendName, appendOptions) {
      const append = window.JL.createConsoleAppender(appendName);
      append.setOptions(appendOptions);

      return append;
    },
    beforeSend(xhr, json) {
      // Provide the whole url to pass queries and # as well.
      json.href = window.location.href;
    },
    test(e) {
      e.preventDefault();
      /* eslint-disable-next-line no-throw-literal */
      throw 'JSNLog test exception';
    },
  };
})(jQuery, Drupal, window);
