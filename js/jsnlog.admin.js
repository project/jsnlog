/**
 * @file
 * JsNlog form behaviors.
 */

(function ($, window, Drupal, once) {
  /**
   * Provide the summary information for the jsnlog settings vertical tabs.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the behavior for the jsnlog settings summaries.
   */
  Drupal.behaviors.jsnlogSettingsSummary = {
    attach() {
      // The drupalSetSummary method required for this behavior is not available
      // on the Blocks administration page, so we need to make sure this
      // behavior is processed only if drupalSetSummary is defined.
      if (typeof $.fn.drupalSetSummary === 'undefined') {
        return;
      }

      /**
       * Create a summary for checkboxes in the provided context.
       *
       * @param {Document|HTMLElement} context
       *   A context where one would find checkboxes to summarize.
       *
       * @return {string}
       *   A string with the summary.
       */
      function checkboxesSummary(context) {
        const values = [];
        const $checkboxes = $(context).find(
          'input[type="checkbox"]:checked + label',
        );
        const il = $checkboxes.length;
        for (let i = 0; i < il; i++) {
          values.push($($checkboxes[i]).html());
        }
        if (!values.length) {
          values.push(Drupal.t('Not restricted'));
        }
        return values.join(', ');
      }

      $(
        '[data-drupal-selector="edit-role-visibility-settings"]',
      ).drupalSetSummary(checkboxesSummary);

      $(
        '[data-drupal-selector="edit-page-visibility-settings"]',
      ).drupalSetSummary((context) => {
        const $pages = $(context).find(
          'textarea[name="visibility_request_path_pages"]',
        );
        if (!$pages.length || !$pages[0].value) {
          return Drupal.t('Not restricted');
        }

        return Drupal.t('Restricted to certain pages');
      });
    },
  };
})(jQuery, window, Drupal, once);
