<?php

declare(strict_types=1);

namespace Drupal\jsnlog\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\jsnlog\JsNlogDownloadInterface;

/**
 * Service for downloading jsnlog library.
 */
class DownloadService implements JsNlogDownloadInterface {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   *  The file system service.
   */
  protected $fileSystem;

  /**
   * The CDN URL.
   *
   * @var string
   *  The CDN URL.
   */
  protected $cdn;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   *  The HTTP client.
   */
  protected $http;

  /**
   * The path to the JSNLog library.
   *
   * @var string
   *  The path to the JSNLog library.
   */
  protected $jsPath = 'libraries/jsnlog/jsnlog.js';

  /**
   * The path to the minified JSNLog library.
   *
   * @var string
   *  The path to the minified JSNLog library.
   */
  protected $minPath = 'libraries/jsnlog/jsnlog.min.js';

  /**
   * The path to the JSNLog library.
   *
   * @var string
   *  The path to the JSNLog library.
   */
  protected $path = 'libraries/jsnlog';

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface $file_system
   *  The file system service.
   * @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @var \GuzzleHttp\Client $http_client
   *   The http client.
   */
  public function __construct(FileSystemInterface $file_system, ConfigFactoryInterface $config_factory, $http_client) {
    $this->fileSystem = $file_system;
    $this->cdn = $config_factory->get('jsnlog.settings')->get('cdn');
    $this->http = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public function download($overwrite = FALSE): void {
    if ($this->hasLibraries() && !$overwrite) {
      return;
    }

    $this->fileSystem->prepareDirectory(
      $this->path,
      FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS
    );
    $cdn = empty($this->cdn) ? self::CDN_FALLBACK : $this->cdn;
    if (!$this->hasMinLibrary() || $overwrite) {
      $this->downloadFile($cdn, self::MIN_PATH);
    }

    if (!$this->hasJsLibrary() || $overwrite) {
      // Regex to replace the .min.js at the end of the URL with .js.
      $jsCdn = preg_replace('/\.min\.js$/', '.js', $cdn);

      $this->downloadFile($jsCdn, self::JS_PATH);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function hasLibraries(): bool {

    $has_js = $this->hasJsLibrary();
    $has_min = $this->hasMinLibrary();

    return $has_js && $has_min;
  }

  /**
   * {@inheritdoc}
   */
  public function hasJsLibrary(): bool {
    $has_js = $this->fileSystem->realPath(self::JS_PATH);
    return (bool) $has_js;
  }

  /**
   * {@inheritdoc}
   */
  public function hasMinLibrary(): bool {
    $has_min = $this->fileSystem->realPath(self::MIN_PATH);
    return (bool) $has_min;
  }

  /**
   * Downloads the JSNLog library.
   *
   * @param string $url
   *   The URL to download.
   * @param string $destination
   *   The destination to save the file.
   */
  protected function downloadFile(string $url, string $destination): void {
    $response = $this->http->get($url, ['sink' => $destination]);
  }

}
