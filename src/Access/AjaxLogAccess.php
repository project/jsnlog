<?php

namespace Drupal\jsnlog\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathMatcher;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\path_alias\AliasManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Checks access for displaying configuration translation page.
 */
class AjaxLogAccess implements AccessInterface {

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $account;

  /**
   * The JSNLog config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private $config;

  /**
   * The path alias manager.
   *
   * @var \Drupal\path_alias\AliasManager
   */
  private $pathManager;

  /**
   * The path matcher.
   *
   * @var \Drupal\Core\Path\PathMatcher
   */
  private $pathMatcher;

  /**
   * The current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  private $pathCurrent;

  /**
   * The current route matcher.
   *
   * @var \Drupal\Core\Path\CurrentRouteMatch
   */
  private $routeCurrent;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  private $moduleHandler;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  private $request;

  /**
   * AjaxLogAccess constructor.
   *
   * @param \Drupal\Core\Session\AccountProxy $account
   *   The current account.
   * @param \Drupal\Core\Config\ConfigFactory $config
   *   The drupal config service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\path_alias\AliasManager $path_manager
   *   The path alias manager.
   * @param \Drupal\Core\Path\PathMatcher $path_matcher
   *   The path matcher.
   * @param \Drupal\Core\Path\CurrentPathStack $path_current
   *   The current path.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $route_current_matcher
   *   The current route matcher.
   * @param \Drupal\Core\Extension\ModuleHandler $module_handler
   *   The module handler.
   */
  public function __construct(AccountProxy $account, ConfigFactory $config, RequestStack $request_stack, AliasManager $path_manager, PathMatcher $path_matcher, CurrentPathStack $path_current, CurrentRouteMatch $route_current_matcher, ModuleHandler $module_handler) {
    $this->account = $account;
    $this->config = $config->get('jsnlog.settings');
    $this->request = $request_stack->getCurrentRequest();
    $this->pathManager = $path_manager;
    $this->pathMatcher = $path_matcher;
    $this->pathCurrent = $path_current;
    $this->routeCurrent = $route_current_matcher->getRouteName();
    $this->moduleHandler = $module_handler;
  }

  /**
   * A custom access check.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account):AccessResult {
    $path = NULL;

    // Get origin path if request is http post method via ajax.
    if ('POST' == $this->request->getMethod() && 'jsnlog.ajax_log' == $this->routeCurrent) {
      $path = $this->getPathFromReferer();
    }

    if ($this->config->get('enabled') && $this->visibilityPages($path) && $this->visibilityRoles($this->account)) {
      return AccessResult::allowed();
    };

    return AccessResult::forbidden();
  }

  /**
   * Tracking visibility check for pages.
   *
   * Based on visibility setting this function returns TRUE if JS code should
   * be added to the current page and otherwise FALSE.
   */
  public function visibilityPages($path = NULL):bool {

    // Cache visibility result if function is called more than once.
    $visibility_request_path_mode = $this->config->get('visibility.request_path_mode');
    $visibility_request_path_pages = $this->config->get('visibility.request_path_pages');
    // Match path if necessary.
    if (!empty($visibility_request_path_pages)) {
      // Convert path to lowercase. This allows comparison of the same path
      // with different case. Ex: /Page, /page, /PAGE.
      $pages = mb_strtolower($visibility_request_path_pages);
      if ($visibility_request_path_mode < 2) {
        // Compare the lowercase path alias (if any) and internal path.
        if (empty($path)) {
          $path = $this->pathCurrent->getPath();
        }

        $path_alias = mb_strtolower($this->pathManager->getAliasByPath($path ?? ''));
        $page_match = $this->pathMatcher->matchPath($path_alias, $pages) || (($path != $path_alias) && $this->pathMatcher->matchPath($path, $pages));
        // When $visibility_request_path_mode has a value of 0, the tracking
        // code is displayed on all pages except those listed in $pages. When
        // set to 1, it is displayed only on those pages listed in $pages.
        $page_match = !($visibility_request_path_mode xor $page_match);
      }
      else {
        $page_match = FALSE;
      }
    }
    else {
      $page_match = TRUE;
    }

    return $page_match;
  }

  /**
   * Tracking visibility check for user roles.
   *
   * Based on visibility setting this function returns TRUE if jsnlog code
   * should be added for the current role and otherwise FALSE.
   *
   * @param object $account
   *   A user object containing an array of roles to check.
   *
   * @return bool
   *   TRUE if JS code should be added for the current role and otherwise FALSE.
   */
  public function visibilityRoles($account):bool {
    $enabled = $visibility_user_role_mode = $this->config->get('visibility.user_role_mode');
    $user_role_roles = $this->config->get('visibility.user_role_roles');

    if (count($user_role_roles) > 0) {
      // One or more roles are selected.
      foreach (array_values($account->getRoles()) as $user_role) {
        // Is the current user a member of one of these roles?
        if (in_array($user_role, $user_role_roles)) {
          // Current user is a member of a role that should be tracked/excluded
          // from tracking.
          $enabled = !$visibility_user_role_mode;
          break;
        }
      }
    }
    else {
      // No role is selected, therefore all roles should be tracked.
      $enabled = TRUE;
    }

    return $enabled;
  }

  /**
   * Get path from referer.
   *
   * @return string
   *   The origin path.
   */
  private function getPathFromReferer():string {
    $referer = $this->request->headers->get('referer');
    // Getting the base url.
    $base_url = Request::createFromGlobals()->getSchemeAndHttpHost();
    // Getting the alias or the relative path.
    return substr($referer, strlen($base_url));
  }

}
