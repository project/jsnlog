<?php

namespace Drupal\jsnlog\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use WhichBrowser\Parser as WhichBrowserParser;

/**
 * Controller routines for AJAX example routes.
 */
class AjaxLogController extends ControllerBase {

  /**
   * The user agent as string or WhichBrowser Class.
   *
   * @var mixed
   */
  private $userAgent;

  /**
   * The payload with data to log.
   *
   * @var array
   */
  private $payload;

  /**
   * {@inheritdoc}
   */
  protected function getModuleName() {
    return 'jsnlog';
  }

  /**
   * Log Data from JSNLog ajax callback.
   *
   * @param Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return Symfony\Component\HttpFoundation\JsonResponse
   *   A basic response.
   */
  public function log(Request $request):JsonResponse {

    $this->userAgent = $request->headers->get('User-Agent');

    if ($this->checkWhichBrowser()) {
      $this->userAgent = new WhichBrowserParser(
        $request->headers->get('User-Agent')
      );
    }

    $payload = $request->getContent();
    $this->payload = json_decode($payload);

    if ($this->processPayload()) {
      return new JsonResponse(['status' => 'logged']);
    }

    return new JsonResponse(['status' => 'failed']);
  }

  /**
   * Process the error object.
   *
   * @return bool
   *   Wether or not we logged successfully.
   */
  private function processPayload():bool {
    if (!empty($this->payload) && !empty($this->payload->lg)) {
      $href = $this->payload->href;

      // Log each error type separate.
      foreach ($this->payload->lg as $logItem) {
        // Get severity.
        $severity = $logItem->l;
        // Handle message inside payload.
        $msg = $this->parseMsg($logItem->m);
        if (is_string($msg)) {
          $this->logStringMessage($msg, $severity, $href);
        }
        else {
          $this->logObjectData($msg, $severity);
        }
      }

      return TRUE;
    }

    return FALSE;
  }

  /**
   * Handle message from payload.
   *
   * @param string $string
   *   The message.
   *
   * @return mixed
   *   Either decoded log_data object or log message.
   */
  private function parseMsg(string $string) {
    if ($this->isJson($string)) {
      $log_data = Json::decode($string);
      $log_data['stack'] = isset($log_data['stack']) ? explode("\n", $log_data['stack']) : [];

      // Attach user agent info.
      $log_data['@user_agent'] = $this->getUserAgent();

      return $log_data;

    }

    return $string;
  }

  /**
   * Write log message to logger.
   *
   * @param string $log_msg
   *   The data to log.
   * @param string $severity
   *   The JSNlog severity.
   * @param string $href
   *   The origin href from the ajax call.
   */
  private function logStringMessage(string $log_msg, string $severity, string $href):void {
    $method = $this->getSeverityMethod($severity);
    $this->getLogger('jsnlog')->{$method}(
      'On page "@href" For "@user_agent" a log entry occurred with severity @severity: "@msg"',
      [
        '@user_agent' => $this->getUserAgent(),
        '@msg' => $log_msg,
        '@severity' => $severity,
        '@href' => $href,
      ]
    );
  }

  /**
   * Write object log data to logger.
   *
   * @param array $log_data
   *   The data to log.
   * @param string $severity
   *   The JSNlog severity.
   */
  private function logObjectData(array $log_data, string $severity):void {
    $base_msg = $this->t(
      'On page "@href" for "@user_agent"<br>
      with window size @windowWidthpx x @windowHeightpx<br>
      at position @windowPositionXpx x @windowPositionYpx an<br>
      @msg occurred with severity @severity<br>
      with message "@errorMsg" in @url:@line_number:@column.
      <br><br>StackTrace:'
    );

    $log_data['@severity'] = $severity;

    $method = $this->getSeverityMethod($severity);
    $msg = $this->processStackTraceForMsg($log_data, $base_msg);
    $this->getLogger('jsnlog')->{$method}($msg, $log_data);
  }

  /**
   * Process stacktrace for logger.
   *
   * @param array $log_data
   *   Log data from request.
   * @param string $msg
   *   Basic message to extend.
   *
   * @return string
   *   The extended log message.
   */
  private function processStackTraceForMsg(array &$log_data, string $msg):string {
    if (is_array($log_data['stack'])) {
      foreach ($log_data['stack'] as $key => $value) {
        $log_data['@stack' . $key] = $value;
        if ($key) {
          $msg .= '<br>&nbsp;&nbsp;@stack' . $key;
        }
        else {
          $msg .= '<br>@stack' . $key;
        }
      }
    }
    unset($log_data['stack']);

    return $msg;
  }

  /**
   * Get severity method by jsnlog severity.
   *
   * @param string $severity
   *   JSNLog severity.
   *
   * @return string
   *   Corresponding logger method.
   */
  private function getSeverityMethod(string $severity):string {
    // Ensure mapping by altering number.
    $severity = preg_split('//', $severity, -1, PREG_SPLIT_NO_EMPTY);
    $severity_count = count($severity);

    switch ($severity_count) {
      case 1:
      case 2:
      case 3:
        $severity = '1';
        break;

      case 4:
        $severity = $severity[0];
        break;

      default:
        // Max severity.
        $severity = '6';
        break;
    }

    $severity_list = $this->getSeverityList();

    if (isset($severity_list[$severity])) {
      return $severity_list[$severity];
    }

    // Fallback if severity mapping failed.
    return 'info';
  }

  /**
   * Return severity list.
   *
   * @return array
   *   List for severity mapping.
   */
  private function getSeverityList():array {
    return [
      '1' => 'log',
      '2' => 'debug',
      '3' => 'info',
      '4' => 'warning',
      '5' => 'error',
      '6' => 'error',
    ];
  }

  /**
   * Check if string is json.
   *
   * @param string $string
   *   String to check.
   *
   * @return bool
   *   Weather or not string is json.
   */
  private function isJson(string $string):bool {
    return is_string($string) && is_array(Json::decode($string, TRUE)) && (json_last_error() == JSON_ERROR_NONE) ? TRUE : FALSE;
  }

  /**
   * Check if WhichBrowser is installed.
   *
   * @return bool
   *   Return weather or not class exists.
   */
  private function checkWhichBrowser():bool {
    if (class_exists(WhichBrowserParser::class)) {
      return TRUE;
    }

    // Try to load.
    $path = getcwd() . '/libraries/whichbrowser/parser/bootstrap.php';
    if (file_exists($path)) {
      require_once getcwd() . '/libraries/whichbrowser/parser/bootstrap.php';

      if (class_exists(WhichBrowserParser::class)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Returns header user agent string.
   *
   * @return string
   *   User Agent string.
   */
  private function getUserAgent():string {
    if (is_string($this->userAgent)) {
      return $this->userAgent;
    }
    return $this->userAgent->toString();
  }

}
