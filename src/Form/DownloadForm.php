<?php

declare(strict_types=1);

namespace Drupal\jsnlog\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\jsnlog\JsNlogDownloadInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for downloading the JSNLog library.
 */
class DownloadForm extends ConfirmFormBase {

  /**
   * The download service.
   *
   * @var \Drupal\jsnlog\JsNlogDownloadInterface
   */
  protected $download;

  /**
   * Constructs a new DownloadForm object.
   *
   * @param \Drupal\jsnlog\JsNlogDownloadInterface $download
   *   The download service.
   */
  public function __construct(JsNlogDownloadInterface $download) {
    $this->download = $download;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('jsnlog.download'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'jsnlog_download_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to download the JSNLog library?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('jsnlog.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {

    $has_js = $this->download->hasJsLibrary();
    $has_min = $this->download->hasMinLibrary();

    $has_both = $has_js && $has_min;
    $missing_both = !$has_js && !$has_min;
    $missing_js = !$has_js && $has_min;
    $missing_min = $has_js && !$has_min;

    if ($has_both) {
      return $this->t('The JSNLog library is available.');
    }
    elseif ($missing_both) {
      return $this->t('The JSNLog library is missing.');
    }
    elseif ($missing_js) {
      return $this->t('The JSNLog library is missing.');
    }
    elseif ($missing_min) {
      return $this->t('The minified JSNLog library is missing.');
    }

    return $this->t('This will download the JSNLog library.');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Download');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->download->download($overwrite = TRUE);
    $form_state->setRedirect('jsnlog.settings');
  }

}
