<?php

namespace Drupal\jsnlog\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\jsnlog\JsNlogDownloadInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure maintenance settings for this site.
 *
 * @internal
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * The library directory.
   */
  public const LIBRARY_DIR = 'libraries/jsnlog';

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * The library discovery service.
   *
   * @var \Drupal\Core\Asset\LibraryDiscovery
   */
  protected $libraryDiscovery;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new SettingsForm.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger service.
   * @param \Drupal\Core\Asset\LibraryDiscoveryInterface $library_discovery
   *   The library discovery service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface|null $typed_config_manager
   *   The typed config manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    MessengerInterface $messenger,
    $library_discovery,
    FileSystemInterface $file_system,
    EntityTypeManagerInterface $entity_type_manager,
    ?TypedConfigManagerInterface $typed_config_manager = NULL,
  ) {
    if (version_compare(\Drupal::VERSION, '10.2.0', '<')) {
      // @phpstan-ignore-next-line
      parent::__construct($config_factory);
    }
    else {
      parent::__construct($config_factory, $typed_config_manager);
    }

    $this->messenger = $messenger;
    $this->libraryDiscovery = $library_discovery;
    $this->fileSystem = $file_system;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $discovery = NULL;
    if (version_compare(\Drupal::VERSION, '11.1.0', '<')) {
      // @phpstan-ignore-next-line
      $discovery = $container->get('library.discovery');
    }
    else {
      $discovery = $container->get('library.discovery.collector');
    }

    return new self(
      $container->get('config.factory'),
      $container->get('messenger'),
      $discovery,
      $container->get('file_system'),
      $container->get('entity_type.manager'),
      $container->get('config.typed') ?? NULL
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'jsnlog_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['jsnlog.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('jsnlog.settings');

    if (!$this->checkLibraryFolder($settings->get('library_type'))) {
      $this->messenger->addWarning($this->t('JSNLog javascript library not present under libraries. Download the library or use the cdn version.'));
    }

    $form = parent::buildForm($form, $form_state);

    $form['actions']['#weight'] = 250;

    $form['enabled'] = [
      '#type' => 'radios',
      '#options' => [
        $this->t('Disabled'),
        $this->t('Enabled'),
      ],
      '#title' => $this->t('Javascript Logging'),
      '#default_value' => $settings->get('enabled'),
      '#description' => $this->t('Enable or disable javascript logging.'),
    ];

    $form['debug_level'] = [
      '#type' => 'select',
      '#options' => $this->getDebugLevelOptions(),
      '#title' => $this->t('Level'),
      '#default_value' => $settings->get('debug_level'),
      '#description' => $this->t('Define the logging level.'),
    ];

    $form['advanced'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Advanced settings'),
      '#title_display' => 'invisible',
      '#weight' => 200,
    ];

    $form['general'] = [
      '#type' => 'details',
      '#title' => $this->t('Library'),
      '#group' => 'advanced',
    ];

    $form['general']['logging_type'] = [
      '#type' => 'radios',
      '#options' => $this->getLogTypeOptions(),
      '#title' => $this->t('Logging Type'),
      '#default_value' => $settings->get('logging_type'),
      '#description' => $this->t('Should be console and ajax errors be logged or only one of them.'),
    ];

    $form['general']['library_type'] = [
      '#type' => 'radios',
      '#options' => $this->getLibraryTypeOptions(),
      '#title' => $this->t('Library Type'),
      '#default_value' => $settings->get('library_type'),
      '#description' => $this->t('Which library type should be loaded.'),
    ];

    $form['general']['user_agent_regex'] = [
      '#title' => $this->t('User Agent Filter'),
      '#type' => 'textfield',
      '#default_value' => $settings->get('user_agent_regex'),
      '#description' => $this->t('If defined, log messages only get processed if this regular expression matches the user agent string of the browser.'),
      '#maxlength' => 500,
    ];
    $cdn_config = $settings->get('cdn');
    $form['general']['cdn'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CDN URL'),
      '#description' => $this->t('Download the JSNLog library or use the CDN version.'),
      '#default_value' => empty($cdn_config) ? JsNlogDownloadInterface::CDN_FALLBACK : $cdn_config,

    ];

    $visibility_request_path_pages = $settings->get('visibility.request_path_pages');

    $form['page_visibility_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Pages'),
      '#group' => 'advanced',
    ];

    if ($settings->get('visibility.request_path_mode') == 2) {
      $form['page_visibility_settings'] = [];
      $form['page_visibility_settings']['visibility_request_path_mode'] = [
        '#type' => 'value',
        '#value' => 2,
      ];
      $form['page_visibility_settings']['visibility_request_path_pages'] = [
        '#type' => 'value',
        '#value' => $visibility_request_path_pages,
      ];
    }
    else {
      $options = [
        $this->t('Every page except the listed pages'),
        $this->t('The listed pages only'),
      ];
      $description = $this->t("Specify pages by using their paths. Enter one path per line.
        The '*' character is a wildcard. Example paths are %blog for the blog page and
        %blog-wildcard for every personal blog. %front is the front page.",
        [
          '%blog' => '/blog',
          '%blog-wildcard' => '/blog/*',
          '%front' => '<front>',
        ]
      );

      $title = $this->t('Pages');

      $form['page_visibility_settings']['visibility_request_path_mode'] = [
        '#type' => 'radios',
        '#title' => $this->t('Add logging to specific pages'),
        '#options' => $options,
        '#default_value' => $settings->get('visibility.request_path_mode'),
      ];
      $form['page_visibility_settings']['visibility_request_path_pages'] = [
        '#type' => 'textarea',
        '#title' => $title,
        '#title_display' => 'invisible',
        '#default_value' => !empty($visibility_request_path_pages) ? $visibility_request_path_pages : '',
        '#description' => $description,
        '#rows' => 10,
      ];
    }

    // Render the role overview.
    $visibility_user_role_roles = $settings->get('visibility.user_role_roles');

    $form['role_visibility_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Roles'),
      '#group' => 'advanced',
    ];

    $form['role_visibility_settings']['visibility_user_role_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Add logging for specific roles'),
      '#options' => [
        $this->t('Add to the selected roles only'),
        $this->t('Add to every role except the selected ones'),
      ],
      '#default_value' => $settings->get('visibility.user_role_mode'),
    ];
    $form['role_visibility_settings']['visibility_user_role_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Roles'),
      '#default_value' => !empty($visibility_user_role_roles) ? $visibility_user_role_roles : [],
      '#options' => array_map('\Drupal\Component\Utility\Html::escape', $this->userRoleNames()),
      '#description' => $this->t('If none of the roles are selected, all users javascript errors will be logged. If a user has any of the roles checked, that user will be tracked (or excluded, depending on the setting above).'),
    ];

    $form['test_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Test'),
      '#group' => 'advanced',
    ];
    $form['test_settings']['test_button'] = [
      '#type' => 'html_tag',
      '#tag' => 'a',
      '#value' => $this->t('Trigger test error'),
      '#attributes' => [
        'onclick' => 'Drupal.behaviors.jsnlog.test(event)',
        'href' => '#',
      ],
    ];
    $form['test_settings']['description'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('Click this link will trigger a javascript exception and should pop up in your logging if the setup is correct.'),
    ];

    $form['#attached']['library'][] = 'jsnlog/admin';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Verify that every path is prefixed with a slash.
    if ($form_state->getValue('visibility_request_path_mode') != 2 && !empty($form_state->getValue('visibility_request_path_pages'))) {
      $pages = preg_split('/(\r\n?|\n)/', $form_state->getValue('visibility_request_path_pages'));
      foreach ($pages as $page) {
        if (strpos($page, '/') !== 0 && $page !== '<front>') {
          $form_state->setErrorByName('visibility_request_path_pages', $this->t('Path "@page" not prefixed with slash.', ['@page' => $page]));
          // Drupal forms show one error only.
          break;
        }
      }
    }

    $form_state->setValue('visibility_request_path_pages', trim($form_state->getValue('visibility_request_path_pages')));
    $form_state->setValue('visibility_user_role_roles', array_filter($form_state->getValue('visibility_user_role_roles')));

    // Check Library type and validate.
    $library_type = $form_state->getValue('library_type');
    if ('ext' !== $library_type) {
      if (!$this->checkLibraryFolder($library_type)) {
        $form_state->setErrorByName('library_type', $this->t('JSNLog library is not present. Download the library or choose library type external.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->config('jsnlog.settings');
    $has_library_type_value_changed = ($settings->get('library_type') != $form_state->getValue('library_type'));

    $settings
      ->set('enabled', $form_state->getValue('enabled'))
      ->set('logging_type', $form_state->getValue('logging_type'))
      ->set('library_type', $form_state->getValue('library_type'))
      ->set('debug_level', $form_state->getValue('debug_level'))
      ->set('user_agent_regex', $form_state->getValue('user_agent_regex'))
      ->set('cdn', $form_state->getValue('cdn'))
      ->set('visibility.request_path_mode', $form_state->getValue('visibility_request_path_mode'))
      ->set('visibility.request_path_pages', $form_state->getValue('visibility_request_path_pages'))
      ->set('visibility.user_role_mode', $form_state->getValue('visibility_user_role_mode'))
      ->set('visibility.user_role_roles', $form_state->getValue('visibility_user_role_roles'))
      ->save();

    // Reset Library Cache if value changed.
    if ($has_library_type_value_changed) {
      if (version_compare(\Drupal::VERSION, '11.1.0', '<')) {
        // @phpstan-ignore-next-line
        $this->libraryDiscovery->clearCachedDefinitions();
      }
      else {
        $this->libraryDiscovery->clear();
      }
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * Returns list of jsnlog append options.
   *
   * @return array
   *   The append option list.
   */
  private function getLogTypeOptions():array {
    return [
      'all' => $this->t('all'),
      'console' => $this->t('Console Logging'),
      'ajax' => $this->t('Ajax Logging'),
    ];
  }

  /**
   * Returns list of debug level options.
   *
   * @return array
   *   The debug level option list.
   */
  private function getDebugLevelOptions():array {
    return [
      1000 => $this->t('Trace'),
      2000 => $this->t('Debug'),
      3000 => $this->t('Info'),
      4000 => $this->t('Warn'),
      5000 => $this->t('Error'),
      6000 => $this->t('Fatal'),
    ];
  }

  /**
   * Returns a list of types to load js library.
   *
   * @return array
   *   The type option list.
   */
  private function getLibraryTypeOptions():array {
    return [
      '' => $this->t('default'),
      'min' => $this->t('minified'),
      'ext' => $this->t('external'),
    ];
  }

  /**
   * Check if JSNLog library is present.
   *
   * @param string $library_type
   *   The selected library type for checking corresponding js library.
   *
   * @return bool
   *   Whether or not library is present.
   */
  private function checkLibraryFolder(string $library_type):bool {
    $dir = static::LIBRARY_DIR;

    if (is_dir($dir)) {
      if (file_exists($dir . '/jsnlog.js') && '' === $library_type) {
        return TRUE;
      }
      if (file_exists($dir . '/jsnlog.min.js') && 'min' === $library_type) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Get user role names.
   */
  protected function userRoleNames() {
    $storage = $this->entityTypeManager->getStorage('user_role');
    $roles = $storage->loadMultiple();
    $role_names = [];
    foreach ($roles as $role) {
      $role_names[$role->id()] = $role->label();
    }

    return $role_names;
  }

}
