<?php

declare(strict_types=1);

namespace Drupal\jsnlog;

/**
 * Interface for downloading the JSNLog library.
 */
interface JsNlogDownloadInterface {

  public const CDN_FALLBACK = 'https://cdnjs.cloudflare.com/ajax/libs/jsnlog/2.30.0/jsnlog.min.js';

  /**
   * The path to the JSNLog library.
   */
  public const JS_PATH = 'libraries/jsnlog/jsnlog.js';

  /**
   * The path to the minified JSNLog library.
   */
  public const MIN_PATH = 'libraries/jsnlog/jsnlog.min.js';

  /**
   * The path to the JSNLog library.
   */
  public const PATH = 'libraries/jsnlog';

  /**
   * Downloads the JSNLog library.
   */
  public function download($overwrite = FALSE): void;

  /**
   * Checks if the library is available.
   *
   * @return bool
   *   TRUE if the library is available, FALSE otherwise.
   */
  public function hasLibraries(): bool;

  /**
   * Checks if the JSNLog library is available.
   *
   * @return bool
   *   TRUE if the JSNLog library is available, FALSE otherwise.
   */
  public function hasJsLibrary(): bool;

  /**
   * Checks if the minified JSNLog library is available.
   *
   * @return bool
   *   TRUE if the minified JSNLog library is available, FALSE otherwise.
   */
  public function hasMinLibrary(): bool;

}
