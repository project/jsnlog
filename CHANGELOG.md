JSNLog 8.x-1.1, 2020-04-26
--------------------------

- 3205561: Fixed case sensitive file name issue. THX to @jeff-veit
- 3129143: Update JSNLog module for drupal ^8.8 and ^9.0 
  (Tested with drupal 9.1) - THX to @kunal_singh.
